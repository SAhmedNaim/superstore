import Vue from 'vue'
import App from './App'

window.$ = window.jQuery = require('jquery')

import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'

Vue.config.productionTip = false

import VueRouter from 'vue-router'

Vue.use(VueRouter)

import { routes } from './router/index'

const router = new VueRouter({
  mode: 'history',
  routes
})

import vuex from 'vuex'

Vue.use(vuex)

import { storage } from './store/index'

const store = new vuex.Store(storage)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
